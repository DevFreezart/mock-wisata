package com.mock.wisata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockWisataApplication {

    public static void main(String[] args) {
        SpringApplication.run(MockWisataApplication.class, args);
    }

}
