package com.mock.wisata.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "wisata")
public class Wisata {

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE
    )
    private Long id;

    @Column(name = "kategori_id")
    private Long kategoriId;

    @Column(name = "nama_wisata")
    private String namaWisata;

    private String slug;

    private BigDecimal harga;

    private String deskripsi;

    private String kota;

    private String provinsi;

    private String alamat;

    @Column(name = "waktu_buka")
    private String waktuBuka;

    private String latitude;

    private String longitude;

    private String image;

    @Column(name = "created_at")
    private String createdAt;

    @Column(name = "updated_at")
    private String updatedAt;

}
