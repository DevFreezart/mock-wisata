package com.mock.wisata.base;

import lombok.Data;

@Data
public class BaseResponse<T> {

    private Integer status;

    private String message;

    private T data;

}
