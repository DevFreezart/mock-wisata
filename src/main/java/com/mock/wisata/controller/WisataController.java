package com.mock.wisata.controller;

import com.mock.wisata.base.BaseResponse;
import com.mock.wisata.entity.Wisata;
import com.mock.wisata.repository.WisataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class WisataController {

    @Autowired
    private WisataRepository wisataRepository;

    @GetMapping(value = "wisata")
    private ResponseEntity getWisata() {

        List<Wisata> wisataList = wisataRepository.findAll();

        BaseResponse<List<Wisata>> response = new BaseResponse();
        response.setStatus(1);
        response.setMessage("Berhasil");
        response.setData(wisataList);

        return ResponseEntity.ok(response);

    }

    @PutMapping(value = "edit-wisata")
    private ResponseEntity putWisata(@RequestBody Wisata request) {

        Wisata wisata = wisataRepository.save(request);

        BaseResponse<Wisata> response = new BaseResponse();
        response.setStatus(1);
        response.setMessage("Berhasil");
        response.setData(wisata);

        return ResponseEntity.ok(response);

    }

    @PostMapping(value = "add-wisata", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    private ResponseEntity postWisata(@RequestBody Wisata request) {

        Wisata wisata = wisataRepository.save(request);

        BaseResponse<Wisata> response = new BaseResponse();
        response.setStatus(1);
        response.setMessage("Berhasil");
        response.setData(wisata);

        return ResponseEntity.ok(response);

    }

    @DeleteMapping(value = "delete-wisata/{id}")
    private ResponseEntity deleteWisata(@PathVariable("id") Long id) {

        wisataRepository.deleteById(id);


        BaseResponse response = new BaseResponse();
        response.setStatus(1);
        response.setMessage("Berhasil");

        return ResponseEntity.ok(response);

    }

}
