package com.mock.wisata.repository;

import com.mock.wisata.entity.Wisata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WisataRepository extends JpaRepository<Wisata,Long> {
}
